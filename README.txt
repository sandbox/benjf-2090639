COMMERCE STRIPE CONNECT
-----------------------

Commerce Stripe Connect provides a block with the Stripe Connect button,
and a menu path ('commerce_stripe_connect/authorize') to handle the Stripe
response and save the vendor API keys in two user fields.

This module also provides a Rules event, triggered when the new account is
successfully connected. This would be a good place to do things like:
  * display a success message
  * update user role(s)
  * redirect the user

Beware:
* You probably want a Rule or custom code to either:
  a. limit cart items to a single vendor, or
  b. split orders and charge each vendor individually


USAGE
-----

1. Create the following text fields on the User entity:
   a. field_stripe_pubkey
   b. field_stripe_token

2. Ensure the Stripe payment method is enabled and your master API keys are
entered on the configuration form.

3. Enter your master Stripe Client ID into the 'STRIPE_CLIENT_ID' constant at
the top of the commerce_stripe_connect.module file.

4. Place the 'Stripe Connect Button' block somewhere visible in your theme.

(At this point, users with the 'Connect Stripe account' permission should see
the block and be able to connect their Stripe account)

5. Patch the Commerce Stripe module to use the vendor api key when appropriate.

find this in commerce_stripe.module:
  Stripe::setApiKey($payment_method['settings']['secret_key']);

replace it with something like this:
  Stripe::setApiKey($vendor_user->field_stripe_token['und'][0]['value']);

6. Optionally, add an 'application fee'. Find the definition of the $c array,
in the same function you just edited above, and add the following:
(like other amounts, this is in pennies/cents. 100 = $1.00)
  'application_fee' => 100, // flat $1.00 fee
 OR
  'application_fee' => ($charge['amount'] * .01), // 1% fee

